
describe('FizzBuzz',()=>{
  let bucle = new Numbers()

  it('Make array with 100 numbers',()=>{
    const number = 101

    const result = bucle.buildArray()

    expect(result.length).toEqual(number)
  })

  it('return array witch starts by 1',()=>{
    const number = "1"
    const result = bucle.buildArray()

    expect(result[1]).toEqual(number)
  })

  it('replace multiple of three by "Fizz"',()=>{
    const fizz = "Fizz"
    const result = bucle.buildArray()

    expect(result[6]).toEqual(fizz)
  })

  it('replace multiple of five by Buzz"',()=>{
    const buzz = "Buzz"
    const result = bucle.buildArray()

    expect(result[5]).toEqual(buzz)
  })

  it('return array witch ends by 100',()=>{
    const buzz = "Buzz"
    const result = bucle.buildArray()

    expect(result[100]).toEqual(buzz)
  })

  it('replace multiple of three and five by FizzBuzz',()=>{
    const fizzbuzz = "FizzBuzz"
    const result = bucle.buildArray()

    expect(result[15]).toEqual(fizzbuzz)
  })

  it('print numbers witch are not multiple by tree or five',()=>{
    const string = '2'
    const  result = bucle.buildArray()

    expect(result[2]).toEqual(string)
  })

})
